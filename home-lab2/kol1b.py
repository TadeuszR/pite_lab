#Tadeusz Raczek
import math
import random
import time
import sys
import termios
import atexit
from select import select
class Samolot:
	omega = 0.5
	alpha = 0.0
	dt = 0.2
	def odchylenie(self, alpha):
		self.alpha += alpha
		print "Wykryto odchylenie: %f, obecnie wynosi %f " % (alpha, self.alpha)
	
	def korekcja(self):
		if abs(self.alpha) < self.omega:
			self.alpha = 0.0
			print "        Odchylenie: %f" % (self.alpha)
		elif self.alpha < 0.0:
			self.alpha += self.omega
		elif self.alpha > 0.0:
			self.alpha -= self.omega
		print "        Odchylenie: %f" % (self.alpha)

	def runda(self):
		while True:
			if self.alpha != 0.0:
				self.korekcja()
			else:
				return
	def run(self):
		time.sleep(self.dt)
		self.korekcja()
		if random.uniform(0.0, 1.0) < 0.1:
			self.odchylenie(random.uniform(-10.0, 10.0))

if __name__ == "__main__":
	s = Samolot()
	s.odchylenie(random.uniform(-20.0, 20.0))
	while True:
		s.run()
			 
