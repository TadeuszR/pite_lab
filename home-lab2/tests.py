import unittest
from kol1b import *
import math

class SamolotTest(unittest.TestCase):

	def testAfterRandom(self):
		randRange = 20.0
		sam = Samolot()
		sam.odchylenie(random.uniform(-randRange, randRange))
		self.assertTrue(-randRange <= sam.alpha <= randRange)
	def testIsZero(self):
		sam = Samolot()
		sam.korekcja();
		self.assertEqual(sam.alpha, 0)
	def testIsZeroAfterOneSeries(self):
		randRange = 20.0
		sam = Samolot()
		sam.odchylenie(random.uniform(-randRange, randRange))
		sam.runda();
		self.assertEqual(sam.alpha, 0)
	def testIfCorrectionWorksProperly(self):
		randRange = 20.0
		sam = Samolot()
		sam.odchylenie(random.uniform(-randRange, randRange))
		before = sam.alpha
		sam.korekcja()
		self.assertTrue(abs(sam.alpha) < abs(before))
	def testType(self):
		randRange = 20.0
		sam = Samolot()
		sam.odchylenie(random.uniform(-randRange, randRange))
		self.assertEqual(type(sam.alpha).__name__, 'float')
		sam.korekcja();
		self.assertEqual(type(sam.alpha).__name__, 'float')
	
if __name__ == "__main__":
	unittest.main()