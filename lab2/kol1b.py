#Tadeusz Raczek
import math
import random
import time
import sys
import termios
import atexit
from select import select
class Samolot:
	omega = 0.5
	alpha = 0.0
	dt = 0.2
	def odchylenie(self, alpha):
		self.alpha = alpha
		print "Wykryto odchylenie: %f" % (self.alpha)
	
	def korekcja(self):
		while True:
			time.sleep(self.dt)
			if abs(self.alpha) < self.omega:
				self.alpha = 0.0
				print "        Odchylenie: %f" % (self.alpha)
				return
			elif self.alpha < 0.0:
				self.alpha += self.omega
			elif self.alpha > 0.0:
				self.alpha -= self.omega
			print "        Odchylenie: %f" % (self.alpha)

	def run(self):
		self.odchylenie(random.uniform(-20.0, 20.0))
		self.korekcja()

if __name__ == "__main__":
	s = Samolot()
	while True:
		s.run()
			 
